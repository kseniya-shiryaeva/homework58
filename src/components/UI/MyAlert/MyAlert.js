import React from 'react';
import './MyAlert.css';

const MyAlert = props => {
    let alertClasses = ['MyAlert'];
    alertClasses.push(props.type);
    return (
        <div
            className={alertClasses.join(' ')}
            style={{
                display: props.show ? 'block' : 'none'
            }}
        >
            <span className="close-button" onClick={props.close}>x</span>
            <div className="alert-body">This is {props.type} alert</div>
        </div>
    );
};

export default MyAlert;