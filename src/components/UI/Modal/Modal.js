import React from 'react';
import './Modal.css';
import Backdrop from "../Backdrop/Backdrop";

const Modal = props => {
    return (
        <>
        <div
            className="Modal"
            style={{
                transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                opacity: props.show ? '1' : '0'
            }}
        >
            <span className="close-button" onClick={props.close}>X</span>
            <h3>{props.title}</h3>
            <div className="modal-body">
                {props.children}
            </div>
        </div>
        <Backdrop show={props.show} clicked={props.close} />
        </>
    );
};

export default Modal;