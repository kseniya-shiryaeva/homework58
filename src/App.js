import React, {useState} from 'react';
import Modal from "./components/UI/Modal/Modal";
import './App.css';
import MyAlert from "./components/UI/MyAlert/MyAlert";

const alertTypes = ['primary', 'success', 'danger', 'warning'];

const App = () => {
    const [showModal, setShowModal] = useState(false);
    const [showAlert, setShowAlert] = useState({
        show: false,
        type: ''
    });

    const toggleModal = () => {
        setShowModal(!showModal);
    }

    const showAlertMessage = () => {
        const typeRandom = Math.floor(Math.random() * alertTypes.length);
        setShowAlert({
            show: true,
            type: alertTypes[typeRandom]
        });
    }

    const closeAlert = () => {
        setShowAlert(prev => ({
            ...prev,
            show: false
        }));
    }

    return (
        <div className="App">
            <Modal title="A title" show={showModal} close={toggleModal}>
                <p>Some content</p>
            </Modal>
            <button className="ShowModalButton" onClick={toggleModal}> Show modal</button>
            <button className="ShowAlertButton" onClick={showAlertMessage}> Show alert</button>
            <MyAlert type={showAlert.type} show={showAlert.show} close={closeAlert} />
        </div>
    );
};

export default App;